<?php 

  /**
   * Installation routine for Public Submit module
   *
   * @package activeCollab.modules.fast_add
   */
  
  $option = new ConfigOption();
  $option->setName('fast_add_default_project');
  $option->setModule('fast_add');
  $option->setType('system');
  $option->setValue(0);
  $option->save();
  
  $option = new ConfigOption();
  $option->setName('fast_add_enabled');
  $option->setModule('fast_add');
  $option->setType('system');
  $option->setValue(false);
  $option->save();
  
  $option = new ConfigOption();
  $option->setName('fast_add_enable_captcha');
  $option->setModule('fast_add');
  $option->setType('system');
  $option->setValue(true);
  $option->save();
  
  $option = new ConfigOption();
  $option->setName('fast_add_enable_description');
  $option->setModule('fast_add');
  $option->setType('system');
  $option->setValue(true);
  $option->save();
  
?>