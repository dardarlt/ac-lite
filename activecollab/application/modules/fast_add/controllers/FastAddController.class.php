<?php
// we need AppicationController
use_controller('application');

/**
   * PublicSubmit Public Controller
   *
   * @package activeCollab.modules.fast_add
   * @subpackage controllers
   */
class FastAddController extends ApplicationController {

	/**
     * Controller name
     *
     * @var string
     */
	var $controller_name = 'fast_add';

	/**
     * Active project (determined in admin settings)
     *
     * @var Project
     */
	var $active_project;

	/**
     * Active project id 
     *
     * @var Project
     */
	var $active_project_id;

	/**
     * Is captcha enabled?
     *
     * @var boolean
     */
	var $captcha_enabled = false;

	/**
     * User is not required to log in
     *
     * @var boolean
     */
	var $login_required = true;

	/**
     * Construct method
     *
     * @param string $request
     * @return PublicSubmitController
     */
	function __construct($request) {
		parent::__construct($request);

		if (!ConfigOptions::getValue('fast_add_enabled')) {
			$this->httpError(HTTP_ERR_NOT_FOUND);
		} // if

		//	$this->mobile_device = mobile_access_module_get_compatible_device(USER_AGENT);
		//	var_dump($this->mobile_device);
		
		$this->setLayout(array(
		"module" => FAST_ADD_MODULE,
		"layout" => 'wireframe',
		));

		$this->active_project_id =   ((string) $this->request->getId('project_id')) ? (string) $this->request->getId('project_id') : ConfigOptions::getValue('fast_add_default_project');

		$this->active_project =  Projects::findById($this->active_project_id);

		//		var_dump($this->active_project);
		//		exit;

		if(!instance_of($this->active_project, 'Project')) {
			$this->httpError(HTTP_ERR_NOT_FOUND);
		} // if


		$this->smarty->assign(array(
			"active_project" => $this->active_project,
			"submit_ticket_url" => assemble_url('fast_add'),
			'captcha_enabled' => $this->captcha_enabled,
			'milestones_url' => assemble_url('fast_add_milestones_url'),
			'assignees_url' => assemble_url('fast_add_assignees_url'),
		));
	} // __construct

	/**
     * Index page action
     *
     */
	function index() {

		$success = flash_get('success');

		$projects = $this->logged_user->getActiveProjects();

		$form_urls = array();
		if(is_foreachable($projects)) {
			foreach($projects as $project) {
				if(Ticket::canAdd($this->logged_user, $project)) {
					$url = $project->getId();
					$form_urls[$url]['name'] = $project->getName();
					if ($this->active_project_id == $project->getId()) $form_urls[$url]['selected'] = 'selected';
				} // if
			} // foreach
		} // if

		//assignees
		$assignees_keys =  Assignments::findAssignmentDataByObject($this->active_project);

		$assignees = array();
		if(is_foreachable($assignees_keys[0])) {
			foreach($assignees_keys[0] as $assignee_key) {
				$assignee_name = Users::findById($assignee_key);

				$name = $assignee_name->values['first_name'] . $assignee_name->values['last_name'] . "[" . $assignee_name->values['email'] . "]";
				$assignees[$assignee_key] = $name ;
			} // foreach
		} // if

		$milestones = array();
		$milestones_keys =  Milestones::findActiveByProject($this->active_project, STATE_VISIBLE, $this->logged_user->getVisibility());
		//$milestones_keys =  Milestones::findActiveByProject($this->active_project, $this->logged_user);
		if(is_foreachable($milestones_keys)) {
			foreach($milestones_keys as $milestone_key) {
				$milestones[$milestone_key->values['id']] =  $milestone_key->values['name'];
			} // foreach
		} // if
		
		$ticket_data = $this->request->post('ticket');
		$this->smarty->assign(array(
		'assignees' => $assignees,
		'milestones' => $milestones,
		'success' => $success,
		'ticket_data' => $ticket_data,
		'form_urls' => $form_urls,
		'ticket_add_url' => assemble_url('fast_add_project', array('project_id' => $this->active_project_id)),
		));

		//fixme . move to models
		$lines = 1;
		$lines =  $this->is_multiline($ticket_data['name']);
		if ($lines)
		{
			$num_lines = count($lines);
		}

		$this->smarty->assign(array(
		"ticket_data" => $ticket_data,
		));

		if ($this->request->isSubmitted())
		{

			$errors = new ValidationErrors();

			if (!$errors->hasErrors()) {

				for ($i = 0; $i < $num_lines; $i++)
				{
					db_begin_work();
					$ticket = new Ticket();

					attach_from_files($ticket);

					if ($lines)
					{
						$ticket_data['name'] = $lines[$i];
					}

					$ticket->setAttributes($ticket_data);

					$ticket->setProjectId($ticket_data['project_id']);
					$ticket->setVisibility(VISIBILITY_NORMAL);

					$ticket->setState(STATE_VISIBLE);
					$ticket->setCreatedBy($this->logged_user);

					$save = $ticket->save();
				}
				if (!$save || is_error($save)) {
					unset($ticket_data['captcha']);
					db_rollback();
					$this->smarty->assign(array(
					'ticket_data' => $ticket_data,
					'errors' => $save,
					));
				} else {

					$subscribers = array($this->logged_user->getId());
					if(is_foreachable(array_var($ticket_data['assignees'], 0))) {
						$subscribers = array_merge($subscribers, array_var($ticket_data['assignees'], 0));
					} else {
						$subscribers[] = $this->active_project->getLeaderId();
					} // if

					Subscriptions::subscribeUsers($subscribers, $ticket);

					db_commit();
					$ticket->ready();

					flash_success('Ticket #:ticket_id has been added [<a href="' . assemble_url('fast_add_ticket_edit', array('project_id' => $ticket_data['project_id'], 'ticket_id' => $ticket->getTicketId())) . '" target="_self">edit</a>]' , array('ticket_id' => $ticket->getTicketId()));
					$this->redirectToUrl(assemble_url('fast_add_tickets', array('project_id' => $ticket_data['project_id'])));
//					$this->redirectToUrl(assemble_url('fast_add_project', array('project_id' => $ticket_data['project_id'])));
				} // if


			}
		} // if
	} // index

	/**
     * Action if fast_add module is not setuped properly
     *
     */
	function unavailable() {

	} // unavailable

	/**
     * Ticket submitted successfully
     *
     */
	function success() {

	} // succes
	
	function quick_jump() {
	} // succes

	function is_multiline($text) {

		$lines = explode("\n", $text);

		if (!is_array($lines)) return false;

		return $lines;
	}

	function starred() {
      
      $this->wireframe->current_menu_item = 'starred_folder';
      if($this->request->isSubmitted()) {
        $action = $this->request->post('action');
        if(!in_array($action, array('unstar', 'unstar_and_complete', 'trash'))) {
          $this->httpError(HTTP_ERR_BAD_REQUEST, 'Invalid action');
        } // if
        
        $objects = ProjectObjects::findByIds($this->request->post('objects'), STATE_VISIBLE, $this->logged_user->getVisibility());
        
        db_begin_work();
        foreach($objects as $object) {
          
          // Unstar selected object
          if($action == 'unstar') {
            $object->unstar($this->logged_user);
            
          // Unstar and marked as completed
          } elseif($action == 'unstar_and_complete') {
            $operation = $object->unstar($this->logged_user);
            if($operation && !is_error($operation)) {
              if($object->can_be_completed) {
                $object->complete($this->logged_user);
              } // if
            } // if
            
          // Move to Trash
          } elseif($action == 'trash') {
            if(!$object->canDelete($this->logged_user)) {
              continue;
            } // if
            
            $object->moveToTrash();
          } // if
          
        } // foreach
        db_commit();
      } // if
      
    	$this->smarty->assign('objects', StarredObjects::findByUser($this->logged_user));
    	
    	if($this->request->get('async')) {
        $this->smarty->display(get_template_path('starred', 'dashboard', SYSTEM_MODULE));
        die();
      } // if
    } // starred

} // PublicSubmitController
?>