<?php
  // we need admin acontroller
  use_controller('admin');
  
  /**
   * Administration settings for Public Submit Module
   * 
   * @package activeCollab.modules.fast_add
   * @subpackage controllers 
   */
  class FastAddAdminController extends AdminController {
    /**
     * Controller name
     *
     * @var string
     */
    var $controller_name='fast_add_admin';
    
    /**
     * Is GD library loaded
     *
     * @var boolean
     */
    var $gd_loaded = true;
    
    /**
     * Construct PublicSubmitAdminController
     * 
     * @param string $request
     * @return PublicSubmitAdminController
     *
     */
    function __construct($request) {
      parent::__construct($request);

      if (!(extension_loaded('gd') || extension_loaded('gd2')) || !function_exists('imagefttext')) {
        $this->gd_loaded = false;
      } // if
      
      $this->smarty->assign(array(
       "fast_add_settings_url" =>  assemble_url('admin_settings_fast_add'),
        'fast_add_url' => assemble_url('fast_add'),
        'fast_add_enabled' => ConfigOptions::getValue('fast_add_enabled'),
        'fast_add_captcha_enabled' => ConfigOptions::getValue('fast_add_enable_captcha'),
        'fast_add_project' => Projects::findById(ConfigOptions::getValue('fast_add_default_project')),
        'gd_loaded' => $this->gd_loaded,
      ));
      return $this;
    } // __construct
    
    /**
     * PublicSubmitAdmin index page
     *
     */
    function index() {
      $fast_add_data = $this->request->post('fast_add');
      if(!is_array($fast_add_data)) {
        $fast_add_data = array(
          'project_id' => ConfigOptions::getValue('fast_add_default_project'),
          'enabled' => ConfigOptions::getValue('fast_add_enabled'),
          'captcha' => ConfigOptions::getValue('fast_add_enable_captcha'),
        );
      } // if
      $this->smarty->assign(array(
        'fast_add_data' => $fast_add_data,
      ));
      
      if ($this->request->isSubmitted()){
        ConfigOptions::setValue('fast_add_default_project', array_var($fast_add_data, 'project_id', null));
        ConfigOptions::setValue('fast_add_enabled', array_var($fast_add_data, 'enabled', null));
        ConfigOptions::setValue('fast_add_enable_captcha', array_var($fast_add_data, 'captcha', null));
        flash_success('Public Submit settings have been updated');
        $this->redirectTo('admin_settings_fast_add');
      } // if
    } // index
    
    
  } // PublicSubmitAdminController