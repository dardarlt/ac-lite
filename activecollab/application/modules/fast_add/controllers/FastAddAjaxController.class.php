<?php
// we need AppicationController
use_controller('application');

/**
   * PublicSubmit Public Controller
   *
   * @package activeCollab.modules.fast_add
   * @subpackage controllers
   */
class FastAddAjaxController extends ApplicationController {

	/**
     * Controller name
     *
     * @var string
     */
	var $controller_name = 'fast_add';


	/**
     * Active project (determined in admin settings)
     *
     * @var Project
     */
	var $active_project;

	/**
     * User is not required to log in
     *
     * @var boolean
     */
	var $login_required = true;

	/**
     * Construct method
     *
     * @param string $request
     * @return PublicSubmitController
     */
	function __construct($request) {
		parent::__construct($request);

		if (!ConfigOptions::getValue('fast_add_enabled')) {
			$this->httpError(HTTP_ERR_NOT_FOUND);
		} // if

		$this->setLayout(array(
		"module" => FAST_ADD_MODULE,
		"layout" => 'ajax',
		));

	} // __construct


	function assignees_ajax() {

		$this->active_ticket = new Ticket();

		$active_project =  (string) $this->request->getId('project_id');
		$this->active_project = Projects::findById($active_project);

		$ticket_id = $this->request->getId('ticket_id');
		if($ticket_id) {
			$this->active_ticket = Tickets::findByTicketId($this->active_project, $ticket_id);
		}
		
		$this->smarty->assign(array(
			"active_project" => $this->active_project,
			'active_ticket'         => $this->active_ticket,
		));

	} // succes

	function milestones_ajax() {

		
		$active_project =  (string) $this->request->getId('project_id');

		$this->active_project = Projects::findById($active_project);

		//$milestones_keys =  Milestones::findActiveByProject($this->active_project, $this->logged_user);
		$milestones_keys =  Milestones::findActiveByProject($this->active_project, STATE_VISIBLE, $this->logged_user->getVisibility());

		//var_dump($milestones_keys);
		
		$milestones = array();

		if(is_foreachable($milestones_keys)) {
			foreach($milestones_keys as $milestone_key) {

				//				var_dump($milestone_key->values['id']);
				//				var_dump($milestone_key->values['name']);

				$milestones[] = '<option value="' . $milestone_key->values['id'] . '">' . $milestone_key->values['name'] . '</option>';
			} // foreach
		} // if
		echo '' . implode("\n", $milestones) . '';

	} // succes

} // PublicSubmitController
?>