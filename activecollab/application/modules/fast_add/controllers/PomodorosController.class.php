<?php
// we need AppicationController
use_controller('application');
define('ITEM_TIME', 25);
/**
   * PublicSubmit Public Controller
   *
   * @package activeCollab.modules.fast_add
   * @subpackage controllers
   */
class PomodorosController extends ApplicationController {

	/**
     * Controller name
     *
     * @var string
     */
	var $controller_name = 'pomodoros';

	/**
     * Active project (determined in admin settings)
     *
     * @var Project
     */
	var $active_project;
	
	var $user;
	
	var $active_ticket;

	/**
     * Active project id 
     *
     * @var Project
     */
	var $active_project_id;


	/**
     * User is not required to log in
     *
     * @var boolean
     */
	var $login_required = true;

	/**
     * Construct method
     *
     * @param string $request
     * @return PublicSubmitController
     */
	
	var $pomodoro;
	
	
	function __construct($request) {
		parent::__construct($request);

		$this->user =& get_logged_user();		
		
		if (!ConfigOptions::getValue('fast_add_enabled')) {
			$this->httpError(HTTP_ERR_NOT_FOUND);
		} // if

		//	$this->mobile_device = mobile_access_module_get_compatible_device(USER_AGENT);
		//	var_dump($this->mobile_device);
		$this->active_project_id =   ((string) $this->request->getId('project_id'));
//
		$this->active_project =  Projects::findById($this->active_project_id);
		
		$ticket_id = $this->request->getId('ticket_id');
		if($ticket_id) {
			$this->pomodoro = Pomodoros::findByTicketId($this->active_project, $ticket_id);
			$this->active_ticket = Tickets::findByTicketId($this->active_project, $ticket_id);
		} // if
		
		if(!instance_of($this->pomodoro, 'Pomodoro')) {
			$this->pomodoro = new Ticket();
		} // i
		
		$this->setLayout(array(
			"module" => FAST_ADD_MODULE,
			"layout" => 'ajax',
		));

	
	} // __construct

	/**
     * Index page action
     *
     */
	function index() 
	{
		
		if(!is_array($pomodoro_attr)) {
			$pomodoro_attr = array(
		    	'tid' => $this->request->getId('ticket_id'),
		    	'pid' => $this->active_ticket->getProjectId(),
		    	'uid' => $this->user->getId(),
		    	'bas' => '326',
		    	'time' => ITEM_TIME *  $pomodoro_data['items'] * 60,
		    );
		} // if
		
		$pomodoro_data =  $this->request->post('pomodoros');
		$pomodoro_attr = array_merge($pomodoro_attr, $pomodoro_data);

		if(!is_array($pomodoro_data)) {
			$pomodoro_data = array(
		    	'items' => $this->pomodoro->getItems(),
		    	'done' => $this->pomodoro->getDone(),
		    	'interuptions' => $this->pomodoro->getInteruptions(),
		    	'notes' => $this->pomodoro->getNotes(),
		    );
		    
		} 
		
//		pre_var_dump($pomodoro_attr);
		//exit();
		 
		if($this->request->isSubmitted()) {
			db_begin_work();
			
			$this->pomodoro->setAttributes($pomodoro_attr);
			$save =  $this->pomodoro->save(); 

			if($save && !is_error($save)) {
			
				db_commit();
				if($this->request->getFormat() == FORMAT_HTML) {
					flash_success('Pomodoros updated', array('ticket_id' => $this->active_ticket->getTicketId()));
					
					if ($this->request->isApiCall() || $this->request->isAsyncCall())
					{
					}
					else 
					{
						$this->redirectToUrl($this->redirectToUrl(assemble_url('fast_add_tickets',  array(
							'project_id' => $this->active_ticket->getProjectId(),
						))));
					}
				} else {
					$this->serveData($this->pomodoro, 'ticket');
				} // if
				
			} else {
				db_rollback();

				if($this->request->getFormat() == FORMAT_HTML) {
					$this->smarty->assign('errors', $save);
				} else {
					$this->serveData($save);
				} // if
			} // if
		}
		
		$this->smarty->assign(array(
			'active_project'   => 	$this->active_project,
			'pomodoros'        => 	$this->pomodoro,
			'pomodoro_data'    => 	$pomodoro_data,
			'active_ticket'    => 	$this->active_ticket
		));
	} // index
	
	function reports()
	{
	
	}
	
	function increase_done()
	{
		
		if(!is_array($pomodoro_attr)) {
			$pomodoro_attr = array(
		    	'tid' => $this->request->getId('ticket_id'),
		    	'pid' => $this->active_ticket->getProjectId(),
		    	'uid' => $this->user->getId(),
		    	'done' => $this->pomodoro->getDone() + 1,
		    );
		} // if
		
		//if($this->request->isSubmitted()) {
			db_begin_work();
			
			$this->pomodoro->setAttributes($pomodoro_attr);
			
			$save =  $this->pomodoro->save(); 
			
			if($save && !is_error($save)) {
				db_commit();
			}
		//}
		
	}

} // PublicSubmitController
?>