<?php

/**
   * Public submit module definition
   *
   * @package activeCollab.modules.fast_add
   * @subpackage models
   */
class FastAddModule extends Module {

	/**
     * Plain module name
     *
     * @var string
     */
	var $name = 'fast_add';

	/**
     * Is system module flag
     *
     * @var boolean
     */
	var $is_system = false;

	/**
     * Module version
     *
     * @var string
     */
	var $version = '0.5';

	// ---------------------------------------------------
	//  Events and Routes
	// ---------------------------------------------------

	/**
     * Define module routes
     *
     * @param Router $r
     * @return null
     */
	function defineRoutes(&$router) {

		$router->map('admin_settings_fast_add', 'admin/tools/fast-add', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add_admin', 'action' => 'index'));

		$router->map('fast_add_starred', 'fast_add_starred', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'starred'));

		//quick jump
		$router->map('fast_add_quick_jump', 'fast_add/quick_jump/:project_id', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'quick_jump'), array('project_id' => '\d+'));
		$router->map('fast_add_milestones_url', 'fast_add/milestones/', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'index'));
		$router->map('fast_add_assignees_url', 'fast_add/assignees/', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'index'));

		// public
		$router->map('fast_add', 'fast_add', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'index'));
		$router->map('fast_add_project', 'fast_add/project/:project_id', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'index'), array('project_id' => '\d+'));
		
		$router->map('fast_add_milestone_project', 'fast_add/milestone/:project_id', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'index'), array('project_id' => '\d+'));

		$router->map('fast_add_unavailable', 'fast_add/unavailable', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'unavailable'));
		$router->map('fast_add_success', 'fast_add/success', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add', 'action' => 'success'));
		$router->map('fast_add_assignees_ajax', 'fast_add/assignees/:project_id', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add_ajax', 'action' => 'assignees_ajax'), array('project_id' => '\d+'));
		$router->map('fast_add_milestones_ajax', 'fast_add/milestones/:project_id', array('module' => FAST_ADD_MODULE, 'controller' => 'fast_add_ajax', 'action' => 'milestones_ajax'), array('project_id' => '\d+'));

		
		$router->map('fast_add_tickets', 'fast_add/:project_id/tickets', array('module' => FAST_ADD_MODULE,  'controller' => 'tickets', 'action' => 'index'), array('project_id' => '\d+'));
		$router->map('fast_add_projects_selected', 'fast_add/projects/:project_id', array('module' => FAST_ADD_MODULE,  'controller' => 'tickets', 'action' => 'index'), array('project_id' => '\d+')); //alternative for breadcrumbs
		$router->map('fast_add_ticket', 'fast_add/:project_id/tickets/:ticket_id', array('module' => FAST_ADD_MODULE, 'controller' => 'tickets', 'action' => 'view'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
		
		$router->map('fast_add_ticket_edit', 'fast_add/:project_id/tickets/:ticket_id/edit', array('module' => FAST_ADD_MODULE,  'controller' => 'tickets', 'action' => 'edit'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
		$router->map('fast_add_ticket_add', 'fast_add/:project_id/tickets/add', array('module' => FAST_ADD_MODULE,  'controller' => 'tickets', 'action' => 'edit'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
		$router->map('fast_add_tickets_add', 'fast_add/:project_id/tickets/add', array('module' => FAST_ADD_MODULE,  'controller' => 'tickets', 'action' => 'add'), array('project_id' => '\d+'));
		
		$router->map('change_project_widget', 'change_project', array('module' => FAST_ADD_MODULE, 'controller' => 'widgets', 'action' => 'jump_to_project'));
		$router->map('fast_add_projects', 'fast_add/projects', array('module' => FAST_ADD_MODULE, 'controller' => 'mobile_access_projects', 'action' => 'index'));

		$router->map('project_object_trash', 'projects/:project_id/objects/:object_id/move-to-trash', array('module' => FAST_ADD_MODULE,  'controller' => 'project_objects', 'action' => 'move_to_trash'), array('project_id' => '\d+', 'object_id' => '\d+'));
		
		// Tasks
		$router->map('fast_add_tasklist', 'fast_add/:project_id/tasklist/:ticket_id', array('module' => FAST_ADD_MODULE, 'controller' => 'tickets', 'action' => 'tasks'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
		$router->map('fast_add_newtask', 'fast_add/:project_id/newtask/:ticket_id', array('module' => FAST_ADD_MODULE, 'controller' => 'tickets', 'action' => 'tasks'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
		
		$router->map('fast_add_tasks_add', 'fast_add/:project_id/tasks/add', array('module' => FAST_ADD_MODULE,  'controller' => 'tasks', 'action' => 'add'), array('project_id' => '\d+'));
		$router->map('fast_add_tasks_reorder', 'fast_add/:project_id/tasks/reorder', array('module' => FAST_ADD_MODULE,  'controller' => 'tasks', 'action' => 'reorder'), array('project_id' => '\d+'));
		$router->map('fast_add_tasks_list_completed', 'fast_add/:project_id/objects/:parent_id/list-completed-tasks', array('module' => FAST_ADD_MODULE,  'controller' => 'tasks', 'action' => 'list_completed'), array('project_id' => '\d+','parent_id' => '\d+'));
		$router->map('fast_add_task', 'fast_add/:project_id/tasks/:task_id', array('module' => FAST_ADD_MODULE,  'controller' => 'tasks', 'action' => 'view'), array('project_id' => '\d+', 'task_id' => '\d+'));
		$router->map('fast_add_task_edit', 'fast_add/:project_id/tasks/:task_id/edit', array('module' => FAST_ADD_MODULE,  'controller' => 'tasks', 'action' => 'edit'), array('project_id' => '\d+', 'task_id' => '\d+'));
		$router->map('fast_add_task_complete', 'fast_add/:project_id/tasks/:task_id/complete', array('module' => FAST_ADD_MODULE,  'controller' => 'tasks', 'action' => 'complete'), array('project_id' => '\d+', 'task_id' => '\d+'));
		$router->map('fast_add_task_open', 'fast_add/:project_id/tasks/:task_id/open', array('module' => FAST_ADD_MODULE,  'controller' => 'tasks', 'action' => 'open'), array('project_id' => '\d+', 'task_id' => '\d+'));

		// Comments
		$router->map('fast_add_comments', 'fast_add/:project_id/comments', array('module' => FAST_ADD_MODULE,  'controller' => 'comments', 'action' => 'index'), array('project_id' => '\d+'));
		$router->map('fast_add_comments_add', 'fast_add/:project_id/comments/add', array('module' => FAST_ADD_MODULE,  'controller' => 'comments', 'action' => 'add'), array('project_id' => '\d+'));
		$router->map('fast_add_comment', 'fast_add/:project_id/comments/:comment_id', array('module' => FAST_ADD_MODULE,  'controller' => 'comments', 'action' => 'view'), array('project_id' => '\d+', 'comment_id' => '\d+'));
		$router->map('fast_add_comment_edit', 'fast_add/:project_id/comments/:comment_id/edit', array('module' => FAST_ADD_MODULE,  'controller' => 'comments', 'action' => 'edit'), array('project_id' => '\d+', 'comment_id' => '\d+'));

		//	Milestones
		$router->map('fast_add_milestones', 'fast_add/:project_id/milestones', array('module' => FAST_ADD_MODULE,  'controller' => 'milestones', 'action' => 'index'), array('project_id' => '\d+'));
		$router->map('fast_add_milestones_archive', 'fast_add/:project_id/milestones/archive', array('module' => FAST_ADD_MODULE,  'controller' => 'milestones', 'action' => 'archive'), array('project_id' => '\d+'));
		$router->map('fast_add_milestones_add', 'fast_add/:project_id/milestones/add', array('module' => FAST_ADD_MODULE,  'controller' => 'milestones', 'action' => 'add'), array('project_id' => '\d+'));
		$router->map('fast_add_milestones_quick_add', 'fast_add/:project_id/milestones/quick_add', array('module' => FAST_ADD_MODULE,  'controller' => 'milestones', 'action' => 'quick_add'), array('project_id' => '\d+'));
		$router->map('fast_add_milestones_export', 'fast_add/:project_id/milestones/export', array('module' => FAST_ADD_MODULE,  'controller' => 'milestones', 'action' => 'export'), array('project_id' => '\d+'));
		$router->map('fast_add_milestone', 'fast_add/:project_id/milestones/:milestone_id', array('module' => FAST_ADD_MODULE,  'controller' => 'milestones', 'action' => 'view'), array('project_id' => '\d+', 'milestone_id' => '\d+'));
		$router->map('fast_add_milestone_edit', 'fast_add/:project_id/milestones/:milestone_id/edit', array('module' => FAST_ADD_MODULE,  'controller' => 'milestones', 'action' => 'edit'), array('project_id' => '\d+', 'milestone_id' => '\d+'));
		$router->map('fast_add_milestone_reschedule', 'fast_add/:project_id/milestones/:milestone_id/reschedule', array('module' => FAST_ADD_MODULE,  'controller' => 'milestones', 'action' => 'reschedule'), array('project_id' => '\d+', 'milestone_id' => '\d+'));

		//CheckLists
		
		$router->map('fast_add_checklists', 'fast_add/:project_id/checklists', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'index'), array('project_id' => '\d+'));
		$router->map('fast_add_list_checklists', 'fast_add/:project_id/list_checklists', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'list_checklists'), array('project_id' => '\d+'));
      $router->map('fast_add_checklists_reorder', 'fast_add/:project_id/checklists/reorder', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'reorder'), array('project_id' => '\d+'));
      $router->map('fast_add_checklists_archive', 'fast_add/:project_id/checklists/archive', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'archive'), array('project_id' => '\d+'));
      $router->map('fast_add_checklists_add', 'fast_add/:project_id/checklists/add', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'add'), array('project_id' => '\d+'));
      $router->map('fast_add_checklists_quick_add', 'fast_add/:project_id/checklists/quick-add', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'quick_add'), array('project_id' => '\d+'));
      $router->map('fast_add_checklists_export', 'fast_add/:project_id/checklists/export', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'export'), array('project_id' => '\d+'));
      
      $router->map('fast_add_checklist', 'fast_add/:project_id/checklists/:checklist_id', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'view'), array('project_id' => '\d+', 'checklist_id' => '\d+'));
      $router->map('fast_add_checklist_edit', 'fast_add/:project_id/checklists/:checklist_id/edit', array('module' => FAST_ADD_MODULE, 'controller' => 'checklists', 'action' => 'edit'), array('project_id' => '\d+', 'checklist_id' => '\d+'));
		
      //fast_add
      $router->map('fast_add_pages', 'fast_add/:project_id/pages', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'index'), array('project_id' => '\d+'));
      $router->map('fast_add_list_pages', 'fast_add/:project_id/list_pages', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'list_pages'), array('project_id' => '\d+'));
      $router->map('fast_add_pages_add', 'fast_add/:project_id/pages/add', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'add'), array('project_id' => '\d+'));
      $router->map('fast_add_pages_quick_add', 'fast_add/:project_id/pages/quick-add', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'quick_add'), array('project_id' => '\d+'));
      $router->map('fast_add_pages_export', 'fast_add/:project_id/pages/export', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'export'), array('project_id' => '\d+'));
      $router->map('fast_add_pages_reorder', 'fast_add/:project_id/pages/reorder', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'reorder'), array('project_id' => '\d+'));
      $router->map('fast_add_pages_categories', 'fast_add/:project_id/pages/categories', array('module' => FAST_ADD_MODULE,  'controller' => 'pages', 'action' => 'categories'), array('project_id' => '\d+'));
      
      $router->map('fast_add_page', 'fast_add/:project_id/pages/:page_id', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'view'), array('project_id' => '\d+', 'page_id' => '\d+'));
      $router->map('fast_add_page_edit', 'fast_add/:project_id/pages/:page_id/edit', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'edit'), array('project_id' => '\d+', 'page_id' => '\d+'));
      $router->map('fast_add_page_revert', 'fast_add/:project_id/pages/:page_id/revert', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'revert'), array('project_id' => '\d+', 'page_id' => '\d+'));
      $router->map('fast_add_page_compare_versions', 'fast_add/:project_id/pages/:page_id/compare-versions', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'compare_versions'), array('project_id' => '\d+', 'page_id' => '\d+'));
      $router->map('fast_add_page_archive', 'fast_add/:project_id/pages/:page_id/archive', array('module' => FAST_ADD_MODULE,  'controller' => 'pages', 'action' => 'archive'), array('project_id' => '\d+', 'page_id' => '\d+'));
      $router->map('fast_add_page_unarchive', 'fast_add/:project_id/pages/:page_id/unarchive', array('module' => FAST_ADD_MODULE, 'controller' => 'pages', 'action' => 'unarchive'), array('project_id' => '\d+', 'page_id' => '\d+'));
      
      $router->map('fast_add_page_version_delete', 'fast_add/:project_id/pages/:page_id/versions/:version/delete', array('module' => FAST_ADD_MODULE, 'controller' => 'page_versions', 'action' => 'delete'), array('project_id' => '\d+', 'page_id' => '\d+', 'version' => '\d+'));
      
      $router->map('fast_add_pomodoros', 'fast_add/:project_id/pomodoros/:ticket_id', array('module' => FAST_ADD_MODULE,  'controller' => 'pomodoros', 'action' => 'index'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
      $router->map('fast_add_pomodoros_increase', 'fast_add/:project_id/pomodoros_increase/:ticket_id', array('module' => FAST_ADD_MODULE,  'controller' => 'pomodoros', 'action' => 'increase_done'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
      $router->map('fast_add_pomodoros_reports', 'fast_add/:project_id/pomodoros_reports/:ticket_id', array('module' => FAST_ADD_MODULE,  'controller' => 'pomodoros', 'action' => 'reports'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
      $router->map('fast_add_run_pomodoro', 'fast_add/:project_id/run_pomodoro/:ticket_id', array('module' => FAST_ADD_MODULE,  'controller' => 'tickets', 'action' => 'run_pomodoro'), array('project_id' => '\d+', 'ticket_id' => '\d+'));
	} // defineRoutes

	/**
     * Define event handlers
     *
     * @param EventsManager $events
     * @return null
     */
	function defineHandlers(&$events) {

		$events->listen('on_admin_sections', 'on_admin_sections');
		//$events->listen('on_project_object_options', 'on_fast_add_quick_options');
	} // defineHandlers

	// ---------------------------------------------------
	//  (Un)Install
	// ---------------------------------------------------

	/**
     * Install this module
     *
     * @param void
     * @return boolean
     */
	function install() {
		$this->addConfigOption('fast_add_default_project', SYSTEM_CONFIG_OPTION, 0);
		$this->addConfigOption('fast_add_enabled', SYSTEM_CONFIG_OPTION, false);
		$this->addConfigOption('fast_add_enable_captcha', SYSTEM_CONFIG_OPTION, true);

		return parent::install();
	} // install

	/**
     * Get module display name
     *
     * @return string
     */
	function getDisplayName() {
		return lang('AC lite');
	} // getDisplayName

	/**
     * Return module description
     *
     * @param void
     * @return string
     */
	function getDescription() {
		return lang('AC lite - lite ActiveCollab');
	} // getDescription

	/**
     * Return module uninstallation message
     *
     * @param void
     * @return string
     */
	function getUninstallMessage() {
		return lang('Module will be deactivated. Data you create using this module will not be deleted');
	} // getUninstallMessage

}

?>