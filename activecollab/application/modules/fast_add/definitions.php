<?php

  /**
   * Public submit module definitions
   *
   * @package activeCollab.modules.fast_add
   */
  
  return array(
  
    // Config options
    'config_options' => array(
      new ConfigOptionDefinition('fast_add_default_project', 'fast_add', 'system', '0'),
      new ConfigOptionDefinition('fast_add_enabled', 'fast_add', 'system', false),
      new ConfigOptionDefinition('fast_add_enable_captcha', 'fast_add', 'system', true),
      new ConfigOptionDefinition('fast_add_enable_description', 'fast_add', 'system', true),
    ),
  
  );

?>