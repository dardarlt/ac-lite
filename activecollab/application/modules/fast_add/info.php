<?php

  /**
   * Module info
   *
   * @package activeCollab.modules.fast_add
   */

  // This file need to be included from the Module object
  if(!isset($this) || !instance_of($this, 'Module')) {
    return;
  } // if
  
  // Set info
  $this->info = array(
    'description' => 'Quickly add tickets using as  standalone application or in Firefox sidebar, bookmark or popup',
    'version' => '0.4.1',
    'important' => 'This module is not active by default. Go to Administration, select AC Lite from Tools section and enable this module',
    'uninstall_message' => 'Module will be deactivated. Data you create using this module will not be deleted.',
  );

?>