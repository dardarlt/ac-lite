<?php

/**
   * object_star helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */

/**
   * Render star for a given object
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
function smarty_function_fast_add_object_pomodoros($params, &$smarty) {

	$object = array_var($params, 'object');
	if(!instance_of($object, 'ProjectObject')) {
		return new InvalidParamError('object', $object, '$object is not valid instance of ProjectObject class', true);
	} // if

	$user = array_var($params, 'user');
	if(!instance_of($user, 'User')) {
		return new InvalidParamError('user', $user, '$user is expected to be an instance of User class', true);
	} // if

	$id = array_var($params, 'id');

//	$pomodoros = Pomodoros::find(array(
//	'conditions' => array('tid = ? AND pid = ? AND  DATE(created) = DATE(NOW())', $object->getFieldValue('ticket_id'),  $object->getFieldValue('project_id')),
//	));
	$pomodoros = Pomodoros::findTodayByTicketId($object, $object->getFieldValue('ticket_id'));
	

	if (empty($pomodoros))
	{
		$pomo = new Pomodoro();
		if(!is_array($pomodoros)) {
			$pomodoros = array(
			'tid' => $object->getFieldValue('ticket_id'),
			'pid' => $object->getFieldValue('project_id'),
			'uid' => $user->getId(),
			'time' => 0,
			'done' => 0,
			'items' => 0,
			'interuptions' => 0,
			);
		} // if

		$pomo->setAttributes($pomodoros);
		$save =  $pomo->save();
	}
	else
	{
		$active_project =  Projects::findById($object->getFieldValue('project_id'));
		//$pomo = Pomodoros::findByTicketId($active_project, $object->getFieldValue('ticket_id'));
		//var_dump($pomo);
		//pre_var_dump($pomo->getItems());
		$result = '';
		
		$result .= '<span class="pomodoros_items">' . $pomodoros->getItems() . "</span> / ";
		$result .= '<span class="pomodoros_done">' . $pomodoros->getDone() . "</span>";
		/*
		$max = max(array($pomo->getDone(), $pomo->getItems()));
		
		for ($i=1; $i <= $max; $i++)
		{
			if ($i <= $pomo->getItems() && $i <= $pomo->getDone() )
			{
				$result .= '[x] ';
			}
			elseif ($i > $pomo->getItems())
			{
				$result .= '0 ';
			}
			else 
			{
				$result .= '[ ] ';
			}
			
		}
		*/

		
		
		if (!$result)
		{
			return 'Add pomodoros';
		}
		
		
		
		return $result;
	}


} // smarty_function_object_star

?>