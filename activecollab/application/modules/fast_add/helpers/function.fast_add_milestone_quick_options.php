<?php

/**
   * object_quick_options helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */

/**
   * Render quick options section for a given object
   * 
   * Parameters:
   * 
   * - object - An instance of ProjectObject, User or Company class
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */

function smarty_function_fast_add_milestone_quick_options($params, &$smarty) {

	$object = array_var($params, 'object');

	//	var_dump($object);
	// 	echo "<pre>";
	// 	var_dump($object);
	
	if(!instance_of($object, 'ProjectObject') && !instance_of($object, 'Company') && !instance_of($object, 'User')) {
		return new InvalidParamError('object', $object, '$object is not valid instance of ProjectObject, Company or User class', true);
	} // if

	$user = array_var($params, 'user');
	if(!instance_of($user, 'User')) {
		return new InvalidParamError('user', $user, '$user is expected to be an instance of User class', true);
	} // if

	$options = $object->getQuickOptions($user);

	if($object->canEdit($user)) {
		$options->Add('edit', array(
		'url' => assemble_url('fast_add_milestone_edit', array('project_id' => $object->getProjectId(),  'milestone_id' => $object->getId())),
		'text' => lang('Edit'),
		));
		
		$options->Add('reschedule', array(
		'url' => assemble_url('fast_add_milestone_reschedule', array('project_id' => $object->getProjectId(),  'milestone_id' => $object->getId())),
		'text' => lang('Reschedule'),
		));
		
	} // if


	if(instance_of($options, 'NamedList') && is_foreachable($options->data)) {
		$smarty->assign('_quick_options', $options);
		return $smarty->fetch(get_template_path('_object_quick_options', null, FAST_ADD_MODULE));
	} else {
		return '';
	} // if

} // smarty_function_object_quick_options

?>