<div class="quick_jump">
	<table>
		<tbody>
			<tr class="odd">
				<td><a href="{assemble route=dashboard}" target="_blank">{lang}Dashboard{/lang}</a></td>
			</tr>
			<tr class="odd"><td><a href="{assemble route=projects}" target="_blank">{lang}Projects{/lang}</a> </td>	</tr>	
			<tr class="odd"><td>Tickets: <a href="{assemble route=project_tickets project_id=$active_project->getId() }" title='Go to selected project &raquo;' target="_blank">{$active_project->getName()|clean}</a> </td>	</tr>	
			<tr class="odd"><td>Milestones: <a href="{assemble route=project_milestones project_id=$active_project->getId() }" title='Go to selected project &raquo;' target="_blank">{$active_project->getName()|clean}</a> </td>	</tr>	
		</tbody>
	<table>
</div>