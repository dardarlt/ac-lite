<script type="text/javascript">
{literal}
	$(document).ready(function () {
		$('#fast_add_name').focus();
		$("h1#page_title").html('{/literal}{$active_project->GetName()}{literal}');
		$("#_tickets").attr({
			href: "{/literal}{assemble route=fast_add_tickets project_id=$active_project->getId() }{literal}",
		});
		$("#_milestones").attr({
			href: "{/literal}{assemble route=fast_add_milestones project_id=$active_project->getId() }{literal}",
		});
//		$("#_tickets").parent().attr("href").html('{/literal}{$active_project->GetName()}{literal}');
	});
{/literal}
</script>
{wrap field=assignees}
	{label for=ticketAssignees}Assignees{/label}
	{select_assignees_inline name='ticket[assignees]' value=$ticket_data.assignees object=$active_ticket project=$active_project choose_responsible=true id=select_asignees_popup users_per_row=1}
{/wrap} 

<div class="col extrawide">
	{wrap field=parent_id}
		{label for=ticketParent}Category{/label}
		{select_category name='ticket[parent_id]' value=$ticket_data.parent_id id=ticketParent module=tickets controller=tickets project=$active_project user=$logged_user}
	{/wrap}
</div>  

<div class="col extrawide">
	{wrap field=tags}
	    {label for=ticketTags}Tags{/label}
	    {select_tags name='ticket[tags]' value=$ticket_data.tags project=$active_project id=ticketTags}
	{/wrap}
</div>  