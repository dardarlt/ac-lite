{form action=$fast_add_url method=POST enctype="multipart/form-data" autofocus=yes }
	{if $success}	
		<p id="success" class="flash">{$success}</p>
	{/if}
	
	<div class="clear"></div>
	<div class="col extrawide">
	 	{wrap field=Project}
	   	 	{label for=project required=yes}Project{/label}
	   	 	
	        <select name="ticket[project_id]" id="categorySelect">
		      {foreach from=$form_urls key=form_url item=project_name}
		        <option value="{$form_url|clean}" {$project_name.selected}>{$project_name.name|clean}</option>
		      {/foreach}
		     </select>
	    {/wrap}
    </div>
    
    <div class="col extrawide">
	    {wrap field=Milestone}
	     	{label for=milestone required=no}Milestone{/label}
	        <select id="milestonesResult" name="ticket[milestone_id]">
	        	 {foreach from=$milestones key=milestone_id item=milestone_name}
		        <option value="{$milestone_id|clean}" {$milestone_name.selected}>{$milestone_name|clean}</option>
		      {/foreach}
	        </select>
	    {/wrap}
	</div>
    
	<div class="clear"></div>
   	<div class="col extrawide">
      {wrap field=name}
	        {label for=name required=yes}Summary{/label}
	        {textarea_field name='ticket[name]' id=fast_add_name class='required resizable'}{$ticket_data.name}{/textarea_field}
      {/wrap}
      </div>  
	 <div class="col extrawide">
	      {if $show_full_body}
		      {wrap field=body}
		        {label for=ticketBody required=no}Full Description{/label}
		        {editor_field name='ticket[body]' id=ticketBody class=''}{$ticket_data.body}{/editor_field}
		        {*text_field name='ticket[body]' value=$ticket_data.body id=ticketBody class=''*}
		      {/wrap}
	      {else}
	      	{wrap field=body id=fast_add_ticket_body}
	      	{label for=fast_add_ticket_body}Full description{/label}
	      	{textarea_field name='ticket[body]' id=fast_add_ticket_body}{$ticket_data.body}{/textarea_field}
	    	{/wrap}
	    {/if}
      </div>  
	
	<div class="clear"></div>
    
    <div class="result" id="result">
      		{wrap field=assignees}
				{label for=ticketAssignees}Assignees{/label}
				{select_assignees_inline name='ticket[assignees]' value=$ticket_data.assignees object=$active_ticket project=$active_project choose_responsible=true id=select_asignees_popup users_per_row=1}
			{/wrap} 
			<div class="col extrawide">
			{wrap field=parent_id}
				{label for=ticketParent}Category{/label}
				{select_category name='ticket[parent_id]' value=$ticket_data.parent_id id=ticketParent module=tickets controller=tickets project=$active_project user=$logged_user}
			{/wrap}
			</div>  
			<div class="col extrawide">
			{wrap field=tags}
			    {label for=ticketTags}Tags{/label}
			    {select_tags name='ticket[tags]' value=$ticket_data.tags project=$active_project id=ticketTags}
			{/wrap}
			</div>  
			
      </div>
      
     <div class="clear"></div>
    <div class="col wide">
	    {wrap field=due_on}
		    {label for=ticketDueOn}Due on{/label}
		    {select_date name='ticket[due_on]' value=$ticket_data.due_on id=ticketDueOn}
	    {/wrap}
  	</div>  
   <div class="col">
	  	{wrap field=priority}
	        {label for=priority required=no}Priority{/label}
	        {select_priority name='ticket[priority]' value=$ticket_data.priority id=priority}
	    {/wrap}
  </div>   

 
      
      
      {*
      {wrap field=attachments}
        {label}Attachments{/label}
        {attach_file}
      {/wrap}
      *}
    <div class="clear"></div>
   
   {wrap_buttons}
    {submit}Submit{/submit}
   {/wrap_buttons}
{/form}