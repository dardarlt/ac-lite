{if is_foreachable($_revisions)}
<div class="resource object_revisions object_section">
  <div class="head">
    <h2 class="section_name"><span class="section_name_span">{lang}Older Versions{/lang}</span></h2>
  </div>
  <div class="body">
    <table class="revisions_table">
      <tbody>
      {foreach from=$_revisions item=_revision}
        <tr class="{cycle values='odd,even'}">
          <td class="star">{object_star object=$_revision user=$logged_user}</td>
          <td class="revision_num"><a href="{$_revision->getViewUrl()}">#{$_revision->getRevisionNum()}</a></td>
          <td class="author">{action_on_by user=$_revision->getCreatedBy() datetime=$_revision->getCreatedOn()}</td>
          <td class="options">
          {if $_revision->canDelete($logged_user)}
            {link href=$_revision->getTrashUrl() title='Move to Trash' class=remove_revision}<img src="{image_url name=gray-delete.gif}" alt="" />{/link}
          {/if}
          </td>
        </tr>
      {/foreach}
      </tbody>
    </table>
  </div>
</div>
{/if}