{title}Edit task{/title}
{add_bread_crumb}Edit task{/add_bread_crumb}

<p>&laquo; {lang name=$active_task_parent->getName()}Back to <a href="{assemble route=fast_add_ticket project_id=$active_project->getId() ticket_id=$active_task_parent->getTicketId()}">:name</a>{/lang}.</p>

{form action=$active_task->getEditUrl() method=post}
  {include_template name=_task_form module=resources controller=tasks}
  {wrap_buttons}
    {submit}Submit{/submit}
  {/wrap_buttons}
{/form}
