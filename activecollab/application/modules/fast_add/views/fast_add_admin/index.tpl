{title}AC Lite Settings{/title}
{add_bread_crumb}AC Lite{/add_bread_crumb}

  <h2 class="section_name"><span class="section_name_span">{lang}Status{/lang}</span></h2>
  <div class="section_container content">
  <p>{lang}When enabled, AC Lite module will let people submit tickets through a form on your website without having to be a registered and logged in the system. This module is great for receiving support requests or getting feedback from users{/lang}.</p>
  <ul>
    {if $fast_add_enabled}
      <li>{lang }AC Lite is <strong>enabled</strong>{/lang}.</li>
      <li>{lang url=$fast_add_url}Submission form is located here: <a href=":url" target="_blank">:url</a>{/lang}.</li>
      
      {if instance_of($fast_add_project,'Project')}
        <li>{lang url=$fast_add_project->getOverviewUrl() project_name=$fast_add_project->getName()}After user submits data, a new ticket is created in <a href=":url">:project_name</a> project.{/lang}</li>
      {else}
        <li>{lang}You need to specify default project for newly created tickets first.{/lang}</li>
      {/if}
      
      {if $gd_not_loaded}
        <li>{lang gd_url='http://www.php.net/manual/en/ref.image.php'}GD Library with FreeType2 is required for captcha protection. <a href=":gd_url">Read more about GD Library</a>{/lang}</li>
      {else}
        {if $fast_add_captcha_enabled}
          <li>{lang}<a href="http://en.wikipedia.org/wiki/CAPTCHA" target="_blank">CAPTCHA</a> protection is <strong>enabled</strong> {/lang}.</li>
        {else}
          <li>{lang}<a href="http://en.wikipedia.org/wiki/CAPTCHA" target="_blank">CAPTCHA</a> protection is <strong>disabled</strong> {/lang}.</li>
        {/if}
      {/if}
    {else}
      <li>{lang }AC Lite is <strong>disabled</strong>{/lang}.</li>
    {/if}
  </ul>
  </div>

  <h2 class="section_name"><span class="section_name_span">{lang}Settings{/lang}</span></h2>
  <div class="section_container">
    {form action=$fast_add_settings_url method=POST}
      <div class="col">
        {wrap field=project_id}
          {label for=project_id required=yes}Project for tickets:{/label}
          {select_project user=$logged_user name=fast_add[project_id] value=$fast_add_data.project_id id=project_id show_all=true}
        {/wrap}
      </div>
      
      <div class="col">
        {wrap field=captcha}
          {label for=captcha required=yes}Enable captcha:{/label}
          {if !$gd_not_loaded}
            {yes_no name='fast_add[captcha]' id='captcha' value=$fast_add_data.captcha}
          {else}
            <span class="object_private">{lang gd_url='http://www.php.net/manual/en/ref.image.php'}GD Library is required for captcha protection. <a href=":gd_url">Read more about GD Library</a>{/lang}</span>
          {/if}
        {/wrap}
      </div>
      
      <div class="col">
        {wrap field=enabled}
          {label for=enabled required=yes}Enable AC Lite:{/label}
          {yes_no name='fast_add[enabled]' id='enabled' value=$fast_add_data.enabled}
        {/wrap}
      </div>
      <div class="clear"></div>
      
      {wrap_buttons}
        {submit}Submit{/submit}
      {/wrap_buttons}
    {/form}
  </div>
