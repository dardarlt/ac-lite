{title}Edit Milestone{/title}
{add_bread_crumb}Edit{/add_bread_crumb}
{if $success}	
		<p id="success" class="flash">{$success}</p>
{/if}
{form action=$milestone_edit method=post}
  {include_template name=_milestone_form module=milestones controller=milestones}
  {wrap_buttons}
    {submit}Submit{/submit}
  {/wrap_buttons}
{/form}