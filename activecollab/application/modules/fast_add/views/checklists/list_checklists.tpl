<div id="list_checklists">
  {if is_foreachable($checklists)}
    <div class="checklists_container">
    {foreach from=$checklists item=checklist}
      <div class="checklist even" id="checklist_{$checklist->getId()}" checklist_id="{$checklist->getId()}">
        <table class="checklists_table">
          <tr>
            <td class="star expander">{image name=expand_collapsed.gif}</td>
            <td>{$checklist->getName()}<br/>
            <div class="list_checklists">
		      	{fast_add_object_tasks object=$checklist force_show=yes}
		    </div>
            </td>
            <td class="stats"><span style="display: none;">{lang open_count=$checklist->countOpenTasks() total_count=$checklist->countTasks()}:open_count open tasks of :total_count tasks in list{/lang}</span></td>
          </tr>
        </table>
        <div class="tasks_container"></div>
      </div>
    {/foreach}
    </div> 
  {else}
    <p class="empty_page">{lang}There are no active checklists here{/lang}. {if $add_checklist_url}{lang add_url=$add_checklist_url}Would you like to <a href=":add_url">create one</a>{/lang}?{/if}</p>
    {empty_slate name=checklists module=checklists}
  {/if}
</div>