
		<div class="form_full_view">
			  <div class="form_left_col">
		 		 {wrap field=body}
			  		{label for=Items required=yes}Items{/label}
				   	{if $pomodoro_data.done > 0}
				    	{text_field value=$pomodoro_data.items class='title required' disabled="disabled"  id=Items}
				    	{text_field name='pomodoros[items]' value=$pomodoro_data.items class='title required' type=hidden  id=Items}
				    {else}
				    	{text_field name='pomodoros[items]' value=$pomodoro_data.items class='title required' id=Items}
				    {/if}
			    {/wrap}
			    
		  		{wrap field=body} 
			   		{label for=Done required=yes}Done{/label}
			    	{text_field name='pomodoros[done]' value=$pomodoro_data.done class='title required' id=Done}    
			    {/wrap} 
			    
				{wrap field=body}
				    {label for=interuptions}Interuptions{/label}
				    {text_field name='pomodoros[interuptions]' value=$pomodoro_data.interuptions class='title' id=interuptions} 
				{/wrap}    
				
		    </div>
		    
		    <div class="form_right_col">
			  	{wrap field=notes}
			    	{label for=notes}Notes{/label}
			     	{textarea_field name='pomodoros[notes]' id=notes class='resizable'}{$pomodoro_data.notes}{/textarea_field}
			 	{/wrap}
			 	<a href="{assemble route=fast_add_pomodoros_increase project_id=$active_project->getId() ticket_id=$active_ticket->getTicketId()}" class="start_pomodoro" >Start Pomodoro</a>
		  	</div>
		  	<div class="clear"></div>
		</div>
	