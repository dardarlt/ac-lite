

{title not_lang=true}{lang}Edit Ticket{/lang} #{$active_ticket->getTicketId()}{/title}
<div style=" right: 20px; float: right; font-size: 10px; margin-right: 30px;">Project: <a href="{assemble route=fast_add_tickets project_id=$active_project->getId() }" title='Go to selected project &raquo;' target="_self">{$active_project->getName()|clean}</a></div>
{add_bread_crumb}Edit{/add_bread_crumb}

 {assign_var name=edit_ticket_url}{assemble route=fast_add_ticket_edit project_id=$active_project->getId() ticket_id=$active_ticket->getTicketId()}{/assign_var}
 
{form action=$edit_ticket_url method=post ask_on_leave=yes class='big_form'}
  {include_template name=_ticket_form module=fast_add controller=tickets}
  {if $request->isAsyncCall()}
   <input type="hidden" name="async" value="1" />
   {/if}
  {wrap_buttons}
    {submit}Submit{/submit}
  {/wrap_buttons}
{/form}