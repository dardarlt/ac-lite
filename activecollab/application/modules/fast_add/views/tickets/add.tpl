{title}New Ticket{/title}
{add_bread_crumb}New Ticket{/add_bread_crumb}
{assign_var name=add_ticket_url}{assemble route=fast_add_ticket_add project_id=$active_project->getId()}{/assign_var}
{form action=$add_ticket_url method=post enctype="multipart/form-data" autofocus=yes ask_on_leave=yes class='big_form'}
  {include_template name=_ticket_form module=fast_add controller=tickets}
  
  {wrap_buttons}
    {submit}Submit{/submit}
  {/wrap_buttons}
{/form}