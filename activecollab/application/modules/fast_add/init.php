<?php

  /**
   * Init fast_add module
   *
   * @package activeCollab.modules.fast_add
   */
  
  define('FAST_ADD_MODULE', 'fast_add');
  define('FAST_ADD_MODULE_PATH', APPLICATION_PATH . '/modules/fast_add');
  
  function fast_add_module_get_view_url(&$object) {
    if (!$object) {
      return false;
    } else if (instance_of($object,'ProjectObject')) {
      return assemble_url('mobile_access_view_'.strtolower($object->getType()), array("project_id" => $object->getProjectId(), "object_id" => $object->getId()));
    } else if (instance_of($object, 'Project')) {
      return assemble_url('fast_add_tickets', array("project_id" => $object->getId()));
    } else if (instance_of($object, 'AnonymousUser')) {
      return 'mailto:'.$object->getEmail();
    } else if (instance_of($object, 'PageVersion')) {
      $page = $object->getPage();
      return assemble_url('mobile_access_view_page_version', array("object_id" => $object->getPageId(), 'version' => $object->getVersion(), 'project_id' => $page->getProjectId()));
    } else {
      return assemble_url('mobile_access_view_'.strtolower(get_class($object)), array("object_id" => $object->getId()));
    }// if
  } // mobile_access_module_get_view_url
  
   function fast_add_module_add_ticket_url($project, $additional_params = null) {
    $params = array('project_id' => $project->getId());

    if($additional_params !== null) {
      $params = array_merge($params, $additional_params);
    } // if
    
    return assemble_url('fast_add_tickets_add', $params);
  } // fast_add_module_add_ticket_url
  
  
    function fast_add_checklists_module_add_checklist_url($project, $additional_params = null) {
    $params = array('project_id' => $project->getId());
    if($additional_params !== null) {
      $params = array_merge($params, $additional_params);
    } // if
    return assemble_url('fast_add_checklists_add', $params);
  } // checklists_module_add_checklist_url
  
    function fast_add_milestones_module_add_url($project) {
    return assemble_url('fast_add_milestones_add', array('project_id' => $project->getId()));
  } // milestones_module_add_url
  
   function fast_add_checklists_module_archive_url($project) {
    return assemble_url('fast_add_checklists_archive', array('project_id' => $project->getId()));
  } // checklists_module_archive_url
  
//  not woeking
//  set_for_autoload(array(
//  	'FastAdd' => FAST_ADD_MODULE_PATH . '/models/FastAdd.class.php',
//  ));

  function fast_add_milestones_module_url($project) {
    return assemble_url('fast_add_milestones', array('project_id' => $project->getId()));
  } // milestones_module_url

  
  /**
   * Return pages section URL
   *
   * @param Project $project
   * @return string
   */
  function fast_add_pages_module_url($project) {
    return assemble_url('fast_add_pages', array('project_id' => $project->getId()));
  } // pages_module_url
  
    /**
   * Return add page ULRL
   *
   * @param Project $project
   * @param array $additional_params
   * @return string
   */
  function fast_add_pages_module_add_page_url($project, $additional_params = null, $parent = null) {
    $params = array('project_id' => $project->getId());
    
    if($additional_params !== null) {
      $parent = array_var($additional_params, 'parent');
      if(instance_of($parent, 'Page') || instance_of($parent, 'Category')) {
        $params['parent_id'] = $parent->getId();
      } // if
      if(isset($additional_params['milestone_id'])) {
        $params['milestone_id'] = $additional_params['milestone_id'];
      } // if
    } // if
    
    return assemble_url('fast_add_pages_add', $params);
  } // pages_module_add_page_url
  
  //require_once(FAST_ADD_MODULE_PATH . '/models/FastAdd.class.php');
   set_for_autoload(array(
    'FastAdd' => FAST_ADD_MODULE_PATH . '/models/FastAdd.class.php',
    'Pomodoro' => FAST_ADD_MODULE_PATH . '/models/Pomodoro.class.php',
    'Pomodoros' => FAST_ADD_MODULE_PATH . '/models/Pomodoros.class.php',
  ));
?>