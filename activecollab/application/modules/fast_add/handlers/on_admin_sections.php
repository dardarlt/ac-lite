<?php

  /**
   * Public Submit module on_admin_section event handler
   *
   * @package activeCollab.modules.fast_add
   * @subpackage handlers
   */

  /**
   * Register tool in administration tools section
   *
   * @param array $sections
   * @return null
   */
  function fast_add_handle_on_admin_sections(&$sections) {
    $sections[ADMIN_SECTION_TOOLS][FAST_ADD_MODULE] = array(
      array(
        'name'        => lang('AC Lite'),
        'description' => lang('Information about AC Lite module'),
        'url'         => assemble_url('admin_settings_fast_add'),
        'icon'        => get_image_url('icon.gif', FAST_ADD_MODULE)
      ),
    );
  } // fast_add_handle_on_admin_sections

?>