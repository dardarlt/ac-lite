<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>{lang company=$owner_company->getName()}AC lite / :company{/lang}</title>
  <link rel="stylesheet" href="{$assets_url}/modules/fast_add/stylesheets/style.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="{$assets_url}/stylesheets/datepicker.css" type="text/css" media="screen"/>
  <link rel="shortcut icon" href="{$assets_url}/modules/fast_add/images/favicon.ico" type="image/x-icon">
   
  <script type="text/javascript" src="{$assets_url}/js.php?{$assets_query_string}"></script>
   {template_vars_to_js}
   <script type="text/javascript">
   if(App.{$request->getModule()} && App.{$request->getModule()}.controllers.{$request->getController()}) {ldelim}
   if(typeof(App.{$request->getModule()}.controllers.{$request->getController()}.{$request->getAction()}) == 'function') {ldelim}
   App.{$request->getModule()}.controllers.{$request->getController()}.{$request->getAction()}();
   {rdelim}
   {rdelim}
    </script>
   
  <script type="text/javascript" src="{$assets_url}/modules/{$request->getModule()}/javascript/jquery.textarearesizer.compressed.js"></script>
  <script type="text/javascript" src="{$assets_url}/modules/{$request->getModule()}/javascript/jquery.hotkeys.js"></script>
  <script type="text/javascript" src="{$assets_url}/modules/{$request->getModule()}/javascript/jquery.pomodoro.js"></script>
  <script type="text/javascript" src="{$assets_url}/modules/{$request->getModule()}/javascript/alajax.js"></script>
  
  <link rel="stylesheet" href="{$assets_url}/modules/{$request->getModule()}/stylesheets/{$request->getController()}.css" type="text/css"/>
  <link rel="stylesheet" href="{$assets_url}/modules/{$request->getModule()}/stylesheets/pomodoro.css" type="text/css"/>

  <meta name="viewport" content="width=300;" />
  
  {page_head_tags}
  
	<script type="text/javascript">
	{literal}
	//jQuery(document).bind('keydown', 'n',function (evt){jQuery('#_n').addClass('dirty'); return false; });

	$(document).ready(function () {
			$.pomodoro.url = $(".start_pomodoro").attr("href"); 
			$.pomodoro.popup = true;
			//$(this).next().css("background", "blue"); //veikia
			$.pomodoro();
	});

	{/literal}
	</script>
</head>

<body>  
<div id="popup">
        {$content_for_layout}
</div>     
</body>
</html>
