<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>{lang company=$owner_company->getName()}AC lite / :company{/lang}</title>
  <link rel="stylesheet" href="{$assets_url}/modules/fast_add/stylesheets/style.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="{$assets_url}/stylesheets/datepicker.css" type="text/css" media="screen"/>
  <link rel="shortcut icon" href="{$assets_url}/modules/fast_add/images/favicon.ico" type="image/x-icon">
   
  <script type="text/javascript" src="{$assets_url}/js.php?{$assets_query_string}"></script>
   {template_vars_to_js}
   <script type="text/javascript">
   if(App.{$request->getModule()} && App.{$request->getModule()}.controllers.{$request->getController()}) {ldelim}
   if(typeof(App.{$request->getModule()}.controllers.{$request->getController()}.{$request->getAction()}) == 'function') {ldelim}
   App.{$request->getModule()}.controllers.{$request->getController()}.{$request->getAction()}();
   {rdelim}
   {rdelim}
    </script>
   
  <script type="text/javascript" src="{$assets_url}/modules/{$request->getModule()}/javascript/jquery.textarearesizer.compressed.js"></script>
  <script type="text/javascript" src="{$assets_url}/modules/{$request->getModule()}/javascript/jquery.hotkeys.js"></script>
  <script type="text/javascript" src="{$assets_url}/modules/{$request->getModule()}/javascript/jquery.pomodoro.js"></script>
  <script type="text/javascript" src="{$assets_url}/modules/{$request->getModule()}/javascript/alajax.js"></script>
  
  <link rel="stylesheet" href="{$assets_url}/modules/{$request->getModule()}/stylesheets/{$request->getController()}.css" type="text/css"/>
  <link rel="stylesheet" href="{$assets_url}/modules/{$request->getModule()}/stylesheets/pomodoro.css" type="text/css"/>

  <meta name="viewport" content="width=300;" />
  
  {page_head_tags}
  
  <script type="text/javascript" charset="utf-8">
  {literal}
  <!--
  $(function () {

  	$.ajaxSetup ({
  		cache: false
  	});
  	var ajax_load = {/literal}"<img src='{$assets_url}/modules/fast_add/images/load.gif' alt='loading...' />";

  	//  load() functions
  	var loadUrl = "{$assignees_url}";
  	var milestonesLoadUrl = "{$milestones_url}";{literal}

  	$("#categorySelect").change(function()
  	{
  		$("#result").html(ajax_load).load(loadUrl +  this.value);
  		$("#milestonesResult").html(ajax_load).load(milestonesLoadUrl +  this.value);
  	});

  	
  	
  });
  //-->
  {/literal}
	</script>
  

	<script type="text/javascript">
	{literal}
	/* jQuery textarea resizer plugin usage */
	$(document).ready(function() {
		$('textarea.resizable:not(.processed)').TextAreaResizer();
	});
	{/literal}
	</script>
	
	<script type="text/javascript">
	{literal}
	//jQuery(document).bind('keydown', 'n',function (evt){jQuery('#_n').addClass('dirty'); return false; });

	$(document).ready(function () {
		
		
		$(document).bind('keydown', 'n', function() {
			var url = $('#_new').attr('href');
			if (url) {
				window.location = url;
			}
		});
		
		$(document).bind('keydown', 'p', function() {
			var url = $('#_projects').attr('href');
			if (url) {
				window.location = url;
			}
		});
		
		$(document).bind('keydown', 'esc', function() {
			history.go(-1);
		});
		$('#fast_add_name, #fast_add_ticket_body').bind('keydown', 'esc', function() {
			history.go(-1);
		});
		
		// VIEW TICKET
		//************************
		$(document).bind('keydown', 't', function() {
			//var url = $('#object_quick_option_new_task > a').attr('href');
			jQuery('#object_quick_option_new_task a').blur();
            jQuery('#object_quick_option_new_task a').focus().click();
			return false;
		});
		
		$(document).bind('keydown', 'c', function() {
			//var url = $('#object_quick_option_new_task > a').attr('href');
			jQuery('#object_quick_option_complete a').blur();
            jQuery('#object_quick_option_complete a').focus().click();
			return false;
		});
		
		$('#fast_add_name').focus();
		
		
		
		$(".task a.task_link").click(function(e){
		
		    //prevent link default action
		    e.preventDefault();
		    
		    //get links href
		    var href = $(this).attr("href");
		
			//veikia
			var task_data = $(this).parents('.task').find('.task_data');
			//task_data.addClass('loaded').load(href);
			
			if( !task_data.hasClass("loaded") ) {     
				
				task_data.addClass('loaded').slideDown("fast").load(href);
				
				
				if( $(this).hasClass("new_task") ) 
				{  
					task_data.addClass('new_task');
					
					// show_form.hide();
		          	// $('.main_object .resource.object_tasks').show();
		          	// form_wrapper.show().focusFirstField();
					// return false;
				}
			}
			else
			{
				task_data.removeClass('loaded').slideUp("fast");
			}
				
		});
		
		$(".option .edit, .task .pomodoro").click(function(e){
		
		    //prevent link default action
		    e.preventDefault();
		    var task_data = $(this).parents('.ticket').find('.task_data');
		    //get links href
		    var href = $(this).attr("href")  + '?skip_layout=1&async=1';
		
			if( !task_data.hasClass("loaded") ) {     
				
				task_data.addClass('loaded').slideDown("fast").load(href);
			}
			else
			{
				task_data.removeClass('loaded').slideUp("fast");
			}
				
		});
		
		
		
		//load inside
		 {/literal}{if $active_project}{literal}
		$("a.inside").click(function () {
		    var slide = $("#dashboard");
		    var href = $(this).attr("rel");
		    //alert(href);
		    if (!slide.data("loaded") || slide.data("page") != href) {
		        slide.addClass('loaded').slideDown("fast").load("http://www.tinkama.lt/projektai/public/index.php/fast_add/{/literal}{$active_project->getId()}{literal}/list_" + href + '?skip_layout=1&async=1');
		        slide.data("loaded", true);
		        slide.data("page", href);
		    }
		    else
		    {
		    	slide.removeClass('loaded').slideToggle("fast");
		    	slide.data("loaded", false);
		    }
		});
		
		
		
		
		//dashboard links
		$("#dashboard .name a, #test .name a").live('click', function () {
			 event.preventDefault();
			
		    var slide = $("#dashboard");
		    var href = $(this).attr("href") + '?skip_layout=1&async=1';

		    if (!slide.data("loaded")) {
		    	alert('now loaded!');
		        slide.addClass('loaded').slideDown("fast").load(href);
		        slide.data("loaded", true);
		    }
		    else if(slide.hasClass("loaded"))
		    {
		    	alert('already has class');
				slide.load(href);
				jQuery("#loading").hide();
				slide.data("loaded", true);
		    }
	    	else
		    {
		    	slide.removeClass('loaded').slideToggle("fast");
		    }
		});
		
		{/literal}{/if}{literal}
		
		//loading
		jQuery("#loading").hide();
		
		jQuery("#loading").bind("ajaxSend", function(){
		   jQuery(this).show();
		}).bind("ajaxComplete", function(){
		   jQuery(this).hide();
		});
		
		$(".start_pomodoro").live("click", function(e){
			e.preventDefault();
			$.pomodoro.url = $(this).attr("href"); 
			$(this).css("background", "#FFE8E8");
			
			//$(this).next().css("background", "blue"); //veikia
			$.pomodoro.done = parseInt($(this).closest('span').find(".pomodoros_done").text());
			$.pomodoro();
		});
		$("#pomodoros_form button").live("click", function(event){//pomodoros forms ajax
			
			$('#pomodoros_form').alajax({
				beforeSend: function (){
					$("#pomodoros_form button").die("click", event);
				},
			    success: function (result){
			    		//console.log(event);
			    	$('#pomodoros_form .message').fadeIn("slow").fadeOut("slow");
			    	
			    }
			});
		});
		
		
		
		/*
		$('#pomodoros_form button').live('click', myLiveEventHandler);
		function myLiveEventHandler(event)
		{
		  if(event.handled !== true)
		  {
		    // put your payload here.
		    // this next line *must* be within this if statement
		    $('#pomodoros_form').alajax({
				beforeSend: function (){
					alert('in');
				},
			    success: function (result){
			    	alert('out');
		    		$('#pomodoros_form .message').fadeIn("slow").css("background", "#CBDEB3").fadeOut("slow");
			    }
			});
		    
		    event.handled = true;
		  }
		  return false;
		}
		*/
		
		 var windowSizeArray = [ "width=60,height=60,toolbar=no",
                                    "width=300,height=400,scrollbars=yes" ];
 
            $(document).ready(function(){
                $('.new_window').click(function (event){
 
                    var url = $(this).attr("href");
                    var windowName = "popUp";//$(this).attr("name");
                    var windowSize = windowSizeArray[$(this).attr("rel")];
 
                    var popup = window.open(url, windowName, windowSize);
 
                    event.preventDefault();
 
                });
            });
		
	});

	{/literal}
	</script>
</head>

<body>  
  
 
  <div id="wrapper">
  	  <div id="loading">Loading..</div>
  	  
      <div id="header">
        <h1 >{lang}AC Lite{/lang} </h1> 
        	
       	   <ul id="overlay_main_menu">
	       	{if $ticket_add_url}
	       		<li><span><a href="{$ticket_add_url}" class="icon_big_add" target="_self" id="_new">{lang}New{/lang}</a></span></li>
	       	{else}
	       		<li><span><a href="{assemble route=fast_add}" class="icon_big_add" target="_self" id="_new">{lang}New{/lang}</a></span></li>	
	       	{/if}
	       	<li><span><a href="{assemble route=fast_add_projects}" class="icon_big_projects" target="_self" id="_projects">{lang}Projects{/lang}</a></span></li>
	        
	       	<li>
				<a href="{assemble route=dashboard}"  class="icon_big_general" target="_blank"><span>{lang}Full{/lang}</span></a>
			</li> 
			
			{if $active_project}
			 <li>
				<span id="quick_jump" class="additional">
					
					<a href="{assemble route=fast_add_quick_jump project_id=$active_project->getId()}">
						<span></span>
					</a>
				</span>
				
				<script type="text/javascript">
				App.fast_add.QuickJump.init('quick_jump');
				</script>
				
			</li>
			{/if}
	      </ul>
	      
      </div>
      
      <div class="clear"></div>
      
    
	 
      {if $active_project}
      		<div id="page_header" class="with_page_details">
	      		<div class="page_info_container">
		      	        <h1 style="float: left;" id="page_title">{$active_project->getName()}</h1>
		      	     
		      	        <!--  Actions start-->
		      	           <div style="float: right; width: 50%; position: relative; right: 0;">
						  {assign var=page_actions value=$wireframe->getSortedPageActions()}
					  	  {if is_foreachable($page_actions)}
					  	    <ul id="page_actions">
						  	    	{if is_foreachable($page_actions)}
								  	    {foreach from=$page_actions key=page_action_name item=page_action name=page_actions}
								  	      {if count($page_actions) == 1}
								  	        <li id="{$page_action_name}_page_action" class="single {if is_foreachable($page_action.subitems)}with_subitems hoverable{else}without_subitems{/if}">
								  	      {else}
								    	      <li id="{$page_action_name}_page_action" class="{if $smarty.foreach.page_actions.first}first{elseif $smarty.foreach.page_actions.last}last {/if} {if is_foreachable($page_action.subitems)}with_subitems hoverable{else}without_subitems{/if}">
								  	      {/if}
								  	     
								  	      {link id=$page_action.id  href=$page_action.url method=$page_action.method confirm=$page_action.confirm not_lang=yes}<span>{$page_action.text|clean} {if is_foreachable($page_action.subitems)}<img src="{image_url name='dropdown_arrow.gif'}" alt="" />{/if}</span>{/link}
								  	        
								  	        {if is_foreachable($page_action.subitems)}
								  	        <ul>
								  	        {foreach from=$page_action.subitems key=page_action_subaction_name item=page_action_subaction}
								  	          {if $page_action_subaction.text && $page_action_subaction.url}
								  	          <li id="{$page_action_subaction_name}_page_action" class="subaction">{link href=$page_action_subaction.url method=$page_action_subaction.method id=$page_action_subaction.id confirm=$page_action_subaction.confirm}{$page_action_subaction.text|clean}{/link}</li>
								  	          {else}
								  	          <li id="{$page_action_subaction_name}_page_action" class="separator"></li>
								  	          {/if}
								  	        {/foreach}
								  	        </ul>
								  	        {/if}
								  	      </li>
								  	  	 {counter name=actions_counter_name assign=actions_counter}
							  	       {/foreach}
							  	  {/if}
					  			</ul>
					  		{/if}
					  		</div>
					  	  <!--  Actions end-->
		      	</div>
	      	</div>
	      	<div class="clear""></div>
	      	<div id="subnav">
	       		<a href="{assemble route=fast_add_tickets project_id=$active_project->getId() }" target="_self" id="_tickets">{lang}Tickets{/lang}</a>
	       		<a href="{assemble route=fast_add_milestones project_id=$active_project->getId() }" target="_self" id="_milestones">{lang}Milestones{/lang}</a>
	       		<a href="{assemble route=fast_add_checklists project_id=$active_project->getId() }" target="_self" id="_checklists">{lang}Checklists{/lang}</a><a href="#"  rel="checklists" class="inside"><img src="{image_url name=icons/dropdown.png module=fast_add }"/></a>
	       		<a href="{assemble route=fast_add_pages project_id=$active_project->getId() }" target="_self" id="_pages">{lang}Pages{/lang}</a><a href="#" rel="pages" class="inside"><img src="{image_url name=icons/dropdown.png module=fast_add }"/></a>
		        
		  </div>
		  
  		  <div id="dashboard"></div>
	  {/if}
	  
	    <ul id="breadcrumbs">
    		    <li class="first"><a href="{assemble route=fast_add_projects}">{lang}Home{/lang}</a>&raquo;</li>
    		    {foreach from=$wireframe->bread_crumbs item=bread_crumb name=_bread_crumb}
    		    <li>
    		    {if $bread_crumb.url}
    		      <a href="{$bread_crumb.url}" title="{$bread_crumb.text|clean}">{$bread_crumb.text|clean|excerpt:20}</a>&raquo;
    		    {else}
    		      <span class="current">{$bread_crumb.text|clean}</span>
    		    {/if}
    		    </li>
    		    {/foreach}
    		  </ul>
      
	  
  	  <div class="clear"></div>
  	 
  	  
      <div class="content_container">
        {$content_for_layout}
      </div>
     
      <div id="footer">
      {if $application->copyright_removed()}
        <p id="copyright">&copy;{year} by {$owner_company->getName()|clean}</p>
      {else}
      	<p id="powered_by"><img src="{image_url name=acpoweredwhite.gif}" alt="powered by activeCollab" /></p>
      {/if}
      	<!-- {benchmark} -->
      </div>
  </div>
</body>
</html>
