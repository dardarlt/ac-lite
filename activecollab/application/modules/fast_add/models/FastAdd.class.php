<?php
/**
   * Project class
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */


class FastAdd extends ProjectObject {

    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    var $auto_increment = 'id'; 
  
	function __construct($id = null) {
		parent::__construct($id);
	} // __construct

	function editBreadCrumbs($breadcrumbs)
	{
		foreach ($breadcrumbs as $breadcrumb)
		{
			if (!strpos($breadcrumb['url'], FAST_ADD_MODULE))
			{
				$breadcrumb['url'] = str_replace("index.php/", "index.php/" . FAST_ADD_MODULE . "/", $breadcrumb['url']);
				//$breadcrumb['url'] = '/';
			}
			$array[] = $breadcrumb;
		}
		return $array;
	}
	
	
}


?>