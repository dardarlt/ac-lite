App.fast_add = {
  controllers : {},
  models      : {}
};

/**
 * Assignments controller behavior
 */
App.fast_add.controllers.tickets = {
  
  /**
   * Assignments index page
   */
  index : function() {
    $(document).ready(function() {
    	
       reindex_tickets_table = function (table) {
        var counter = 0;
        table.find('li').each(function () {
          var row = $(this);
          if (row.html() && !row.is('.empty_row') && !row.is('.ui-sortable-helper')) {
            if ((counter % 2) == 0) {
              row.removeClass('even');
              row.addClass('odd');
            } else {
              row.removeClass('odd');
              row.addClass('even');
            } // if
            counter ++;
          } // if
        });
        if (counter<1) {
          table.find('.empty_row').show();
        } else {
          table.find('.empty_row').hide();
        } // if
      } // reindex_tickets_table	
    	
      if (App.data.can_manage_tickets) {
        $('.tickets_list').sortable({
          axis : 'y',      
          cursor: 'move',
          delay: 3,
          placeholder: 'drag_placeholder',
          forcePlaceholderSize : true,
          revert: false,
          connectWith: ['.tickets_list'],
          items: 'li.sort',
          update : function (e, ui) {
            var sortable_list = ui.item.parent();
            reindex_tickets_table(sortable_list);
            var reorder_data = sortable_list.find('input').serialize();
            if (reorder_data) {
              reorder_data+='&submitted=submitted';
              $.ajax({
                type: "POST",
                url: sortable_list.attr('reorder_url'),
                data: reorder_data
              });
            } // if
          },
          over: function (table_object,ui) {
            $(this).addClass('dragging');
          },
          out: function (table_object,ui) {
            $(this).removeClass('dragging');
          }
        });
      } // if
      
      
      $('#assignments_filter_select select').change(function() {
        var filter_url = $(this).val();
        if(filter_url != location.href) {
          $(this).after(' ' + App.lang('Loading ...')).attr('disabled', 'disabled');
          location.href = filter_url;
        } // if
      });
      
      $('#assignments_filter_options a').hover(function() {
        $('#assignments_filter_options span.tooltip').text($(this).attr('title'));
      }, function() {
        $('#assignments_filter_options span.tooltip').text('');
      }); 
      
      $('#toggle_filter_details').click(function() {
        $('#assignments_filter_details').toggle('fast');
        return false;
      });
      
      // Complete / Move to trash tasks
      $('#assignments a.complete_assignment, #assignments a.remove_assignment').click(function() {
        var link = $(this);
        
        // Block additional clicks
        if(link[0].block_clicks) {
          return false;
        } else {
          link[0].block_clicks = true;
        } // if
        
        var img = link.find('img');
        var old_src = img.attr('src');
        
        img.attr('src', App.data.indicator_url);
        
        $.ajax({
          url     : link.attr('href'),
          type    : 'POST',
          data    : {'submitted' : 'submitted'},
          success : function(response) {
            link.parent().parent().parent().remove();
            
            var counter = 1;
            $('#assignments li.assignment_row').each(function() {
              var new_class = counter % 2 == 1 ? 'odd' : 'even';
              $(this).removeClass('even').removeClass('odd').addClass(new_class);
              counter++;
            });
          },
          error   : function() {
            img.attr('src', old_src);
            link[0].block_clicks = false;
          }
        });
        
        return false;
      });
    });
  }
  
}


App.fast_add.QuickJump = function() {
  
  /**
   * Manage categories tab used to initialize the popup
   *
   * This value is present only on pages where we have categories tabs
   *
   * @var jQuery
   */
  var quick_jump_tab = false;
  
 
  
  // Public interface
  return {
    
    /**
     * Initialize manage categories popup
     *
     * @param String list_item_id
     */
    init : function(list_item_id) {
      quick_jump_tab = $('#' + list_item_id); // Remember manage categories tab!
      
      var link = quick_jump_tab.find('a');
      
      link.click(function() {
        var open_url = App.extendUrl(link.attr('href'), {
          skip_layout : 1,
          async : 1
        });
        
        App.ModalDialog.show('manage_categories_popup', App.lang('Jump to full version'), $('<p>' + App.lang('Loading...') + '</p>').load(open_url), {
        	
        	 width:  300,
	          buttons : [{
	            label : App.lang('Close')
	          }]    
        	
        });
        return false;
      });
    },
    
  };
  
}();

App.checklists = {
  controllers : {},
  models      : {}
};

/**
 * Checklists
 */
App.fast_add.controllers.checklists = { 
   
  index : function () {   
    $(document).ready(function() {
      $('td.expander a').click(function () {
        var anchor = $(this);
        var anchor_row = anchor.parents('tr:first');
        var anchor_image = anchor.find('img');
        var ajax_url = App.extendUrl(anchor.attr('href'), {show_only_tasks : 'true', async : 1})
        var checklist_tasks_row = anchor.parents('div.checklist').find('.tasks_container:first');
        
        if (anchor.is('.collapsed')) {
          anchor.removeClass('collapsed');
          anchor_row.removeClass('collapsed');
          anchor_image.attr('src', App.data.indicator_url);
          if (!checklist_tasks_row.html()) {
            $.ajax({
              url : ajax_url,
              success : function (response) {
                anchor.addClass('expanded');
                anchor_row.addClass('expanded');
                anchor_row.removeClass('collapsed');
                anchor_image.attr('src', App.data.expander_expanded);
                checklist_tasks_row.hide();
                checklist_tasks_row.html(response);
                checklist_tasks_row.slideDown();
              },
              error : function(response) {
                anchor_image.attr('src', App.data.expander_collapsed);
              }
            });
          } else {
            anchor.addClass('expanded');
            anchor_row.addClass('expanded');
            anchor_row.removeClass('collapsed');
            anchor_image.attr('src', App.data.expander_expanded);
            checklist_tasks_row.slideDown();
          } // if
        } else if (anchor.is('.expanded')) {
          anchor.addClass('collapsed');
          anchor_row.addClass('collapsed');
          anchor_row.removeClass('expanded');
          anchor_image.attr('src', App.data.expander_collapsed);
          checklist_tasks_row.slideUp();
        } // if
        return false;
      });
      
       // Complete / Move to trash tasks
      $('.checklist a.complete_assignment, .checklist a.remove_assignment').click(function() {
        var link = $(this);
        
        // Block additional clicks
        if(link[0].block_clicks) {
          return false;
        } else {
          link[0].block_clicks = true;
        } // if
        
        var img = link.find('img');
        var old_src = img.attr('src');
        
        img.attr('src', App.data.indicator_url);
        
        $.ajax({
          url     : link.attr('href'),
          type    : 'POST',
          data    : {'submitted' : 'submitted'},
          success : function(response) {
            link.parent().parent().parent().remove();
            
            var counter = 1;
            $('#assignments li.assignment_row').each(function() {
              var new_class = counter % 2 == 1 ? 'odd' : 'even';
              $(this).removeClass('even').removeClass('odd').addClass(new_class);
              counter++;
            });
          },
          error   : function() {
            img.attr('src', old_src);
            link[0].block_clicks = false;
          }
        });
        
        return false;
      });
      
      if(App.data.can_manage_checklists) {
        $('#checklists .checklists_container').sortable({
          items: 'div.checklist',
          axis: 'y',
          distance: '3',
          handle: 'table:first',
          update: function (event, ui) {
            var ajax_data = {
              'submitted' : 'submitted'
            };
            
            var counter = 0;
            $('#checklists div.checklists_container div.checklist:not(.ui-sortable-placeholder)').each(function () {
              ajax_data['checklists[' + counter + ']'] = $(this).attr('checklist_id');
              counter++;
            });
            $.ajax({
              type : 'post',
              data : ajax_data,
              url : App.extendUrl(App.data.reorder_checklists_url, { async : 1 })
            })
          }
        });
        $('#checklists .checklists_container .checklist:not(.ui-sortable-placeholder) table').css('cursor', 'move');
      } // if
    });
  }
};

App.widgets.reorder_checklists = function() {
  var reorder_list;
  var reorder_form;

  return {
    init : function(list_id) {
      reorder_list = $('#' + list_id);
      reorder_form = reorder_list.parents('form');
      
      var list_container = $('#checklists .checklists_container:first');
      var list_prefix = 'checklist_';
      
      reorder_list.sortable({
        axis        : 'y'
      });
      
      reorder_form.find('.buttonHolder button').click(function () {
        reorder_form.block();
        reorder_form.ajaxSubmit({
          method  : 'post',
          url  : App.makeAsyncUrl(reorder_form.attr('action')),
          success : function (response) {
            reorder_form.find('input').each(function () {
              list_container.append($('#'+list_prefix+$(this).val()));
            });
            App.ModalDialog.close();
          },
          error : function (response) {
            reorder_form.unblock();
          }
        });
      });
    }
  }
}();

	